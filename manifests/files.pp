define lab_elasticsearch6x::files(
    $clustername = "default",
    $jvmxms = '1g',
    $jvmxmx = '1g',
){
	File {
		ensure => file,
		owner => "root",
		group => "elasticsearch",
		mode => "0660",
	}
	file {"/etc/elasticsearch/${title}":
		content => template("lab_elasticsearch6x/${title}.erb"),
		#notify => Service['elasticsearch'],
	}
}
