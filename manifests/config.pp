class lab_elasticsearch6x::config {
    
### Resources ###

    if $::os['family'] == 'Redhat' {
   	notify {'SucessNotify':
	    message => "Operation System OK...",
	} 
        
	package {'elasticsearch':
            ensure => "present",
            require => File['/etc/yum.repos.d/elasticsearch.repo'],
        }
        file {'/etc/yum.repos.d/elasticsearch.repo':
            ensure => file,
            source => "puppet:///modules/lab_elasticsearch6x/elasticsearch.repo",
	    mode => "0644",
        }
    
    } else {
	notify {'FailedNotify':
	   message => "Operation System Not Fould...",
	}
    } 
   


   lab_elasticsearch6x::files {'elasticsearch.yml':
	clustername => "Cluster 01",
   }
   lab_elasticsearch6x::files {'jvm.options':
   	jvmxms => '512m',
	jvmxmx => '512m',
	
   }
}

